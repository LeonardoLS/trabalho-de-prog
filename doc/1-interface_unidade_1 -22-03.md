#Rags War

##**PERSONAGENS**
###**1. Curadora**
Habilidades: Este personagem tem a capacidade de auxiliar seus companheiros de guerra oferecendo suporte médico.
Defeito: em meio a uma guerra, é díficil obter itens medicinais e pior ainda quando se tem um limite ínfimo para estocar.             

###**2. Capitão**
Habilidades: Este personagem é responsável por organizar as estratégias de guerra e liderar o exército.
Defeito: Tudo que um capitão precisa é liderar e ter estratégias excepcionais, e caso ele não faça isso direito, todos pagarão.

###**3. Cavaleiro**
Habilidades: Este personagem é um guerreiro que faz a conquista e defesa de territórios.
Defeito: Um cavaleiro não é um cavaleiro sem o seu cavalo, que pode ser pego pelo solerte.

###**4. Solerte**
Habilidades: Este personagem trabalha de forma furtiva infiltrando-se no exército inimigo e saqueando itens de cura e guerra.
Defeito: Sua ganância pelas coisas alheias pode acabar lhe complicando caso o Capitão pegue-o.


##Máximo == 100

| Personagens | Ataque | Defesa | 
|:-----------:|:------:|:------:|
|   Curadora  |   10   |   90   |  
|   Capitão   |   80   |   90   |  
|  Cavaleiro  |   90   |   100  |  
|   Solerte   |   65   |   80   |   


#DESENVOLVIMENTO
##Estrutura da mensagem de commit
    MÓDULO. TIPO (Etiqueta): descrição
    **Módulo:**
    -->PROG: Mudanças feitas no código do RPG;
    -->DOC: Mudanças na documentação.

    **Tipo:**
    -->FIX: Se houver uma Issue relacionada, deve-se fechá-la. Concerto de erros e bugs;
    -->REFACT: Algumas melhorias de código (Sejam elas de performance, de limpeza, ou de documentação);
    -->LAYOUT: Mudanças exclusivas da aparência do jogo;
    -->FEAT: Commits com adição de funcionalidade (Seja ela pronta ou em progresso).

    **Etiqueta:**
    -->A etiqueta é o alvo de modificações do commit. Pode ser um arquivo, ou um módulo da aplicação.

    **Descrição:**
    -->Mensagem curta acerca das implementações do commit.

    ##Exemplos de commits utilizando a estrutura descrita:
    PROG. FEAT (Tutorial):  Add nova tela de descrição dos personagens
    DOC. FEAT (Interface e Unidade): Add novo personagem
    PROG. LAYOUT (Tutorial e Jogar): Ambientalização ao tema


###Link para o vídeo
https://youtu.be/XuQ_30eaA6g