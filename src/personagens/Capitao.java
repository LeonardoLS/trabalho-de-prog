package personagens;

import rpg.Personagem;

/**
 * Classe capitão com herança do Personagem
 * @author larac e kamillek
 */
public class Capitao extends Personagem{
    /**
     * Ataque especial do personagem
     * @param ataque 
     */
    @Override
    public void ataqueEspecial(int ataque){
        //capitão e curandeira
        int nAtak = ataque + 1;
        this.setAtaque(nAtak);
    }
}
