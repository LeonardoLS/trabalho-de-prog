package personagens;

import rpg.Personagem;

/**
 * Classe cavaleiro com herança do Personagem
 * @author larac e kamillek
 */
public class Cavaleiro extends Personagem{
    /**
     * Ataque especial do personagem
     * @param ataque 
     */
    @Override
    public void ataqueEspecial(int ataque){
        //cavaleiro e solerte
        int nAtk = ataque + 2;
        this.setAtaque(nAtk);
    }
}
