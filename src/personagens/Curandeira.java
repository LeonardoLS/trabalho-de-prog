package personagens;

import rpg.Personagem;

/**
 * Classe curandeira com herança do Personagem
 * @author larac e kamillek
 */
public class Curandeira extends Personagem{
    /**
     * Ataque especial do personagem
     * @param defesa
     */
    @Override
    public void ataqueEspecial(int defesa){
        //curandeira e solerte
        int nDef = defesa + 3;
        this.setAtaque(nDef);
    }
}
