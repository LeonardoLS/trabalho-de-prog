package personagens;

import rpg.Personagem;

/**
 * Classe solerte com herança do Personagem
 * @author larac e kamillek
 */
public class Solerte extends Personagem{
    /**
     * Ataque especial do personagem
     * @param ataque 
     */
    @Override
    public void ataqueEspecial(int ataque){
        //solerte e curandeira
        int nAtak = ataque + 1;
        this.setAtaque(nAtak);
    }
}
