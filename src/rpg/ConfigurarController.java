package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * Controller da tela de configuração de ataque e defesa dos personagens
 * @author larac e kamillek
 */
public class ConfigurarController implements Initializable {

    @FXML
    public TextField acav;
    @FXML
    public TextField acap;
    @FXML
    public TextField acur;
    @FXML
    public TextField asol;
    @FXML
    public TextField dcav;
    @FXML
    public TextField dcap;
    @FXML
    public TextField dcur;
    @FXML
    public TextField dsol;
    
    //Ataque e defesa de cada personagem
    public static int atcav, atcap, atcur, atsol;
    public static int dtcav, dtcap, dtcur, dtsol;
    
    /**
     * Obtém os dados da configuração de unidades de ataque e defesa por personagem
     */
    public void getConfig(){
        atcav = Integer.parseInt(acav.getText());
        atcap = Integer.parseInt(acap.getText());
        atcur = Integer.parseInt(acur.getText());
        atsol = Integer.parseInt(asol.getText());
        System.out.println("Ataque:\nCav: "+atcav);
        System.out.println("Cap: "+atcap);
        System.out.println("Cur: "+atcur);
        System.out.println("Sol: "+atsol);
        rpg.Personagem.configurarPersonagens();
        
        dtcav = Integer.parseInt(dcav.getText());
        dtcap = Integer.parseInt(dcap.getText());
        dtcur = Integer.parseInt(dcur.getText());
        dtsol = Integer.parseInt(dsol.getText());
        System.out.println("Defesa:\nCav: "+dtcav);
        System.out.println("Cap: "+dtcap);
        System.out.println("Cur: "+dtcur);
        System.out.println("Sol: "+dtsol);
        rpg.Personagem.configurarPersonagens();
    }
    
    /**
     * Troca para a tela inicial
     * @param event 
     */
    public void menu (ActionEvent event){
        System.out.println("Botão menu!");
        getConfig();
        Rpg.trocaTela("TelaInicial.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
