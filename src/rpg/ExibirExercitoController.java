package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Controller da tela que exibe os dados dos exércitos criados e permite passar para a tela com o resultado da batalha.
 * @author larac e kamillek
 */
public class ExibirExercitoController implements Initializable {
    @FXML
    private Label quantidades;
    
    @FXML
    private Label quantidades2;
    
    /**
     * Troca para a tela que exibe os resultados da batalha
     * @param event 
     */
    @FXML
    private void proximo(ActionEvent event) {
        Rpg.trocaTela("Batalhar.fxml");
    }
    
    public Label getQuantidades() {
        return quantidades;
    }
    public void setQuantidades(Label quantidades) {
        this.quantidades = quantidades;
    }
    public Label getQuantidades2() {
        return quantidades2;
    }
    public void setQuantidades2(Label quantidades2) {
        this.quantidades2 = quantidades2;
    }
    
    /**
     * Exibe a quantidade de cadaa personagem nos exércitos criados
     */
    private void escreverExercito(){
        quantidades.setText("Cavaleiros: "+ rpg.JogarController.e.tamCavaleiro() +"\nCapitões: "+ rpg.JogarController.e.tamCapitao() + "\nCurandeiras: "+ rpg.JogarController.e.tamCurandeira() + "\nSolertes: "+ rpg.JogarController.e.tamSolerte());
        quantidades2.setText("Cavaleiros: "+ rpg.JogarController.e2.tamCavaleiro() +"\nCapitões: "+ rpg.JogarController.e2.tamCapitao() + "\nCurandeiras: "+ rpg.JogarController.e2.tamCurandeira() + "\nSolertes: "+ rpg.JogarController.e2.tamSolerte());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.escreverExercito();
    }
}
