package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Controller da tela que obtém as quantidades de cada personagem, certifica que a quantidade seja igual à 100 e chama as funções para criação das listas dos exércitos.
 * @author larac e kamillek
 */
public class JogarController implements Initializable {
    @FXML
    private TextField cavaleiroField;
    
    @FXML
    private TextField capitaoField;
    
    @FXML
    private TextField curandeiraField;
    
    @FXML
    private TextField solerteField;
    
    @FXML
    private Label textoErro;
    
    //objetos da classe Exército
    public static Exercito e = new Exercito();
    public static Exercito e2 = new Exercito();
    
    //quantidade de cada personagem a ser criado
    static int quantCavaleiro;
    static int quantCapitao;
    static int quantCurandeira;
    static int quantSolerte;
    //variável que certifica que o total de personagens seja igual a 100
    int total = 0;

    public TextField getCavaleiroField() {
        return cavaleiroField;
    }
    public void setCavaleiroField(TextField cavaleiroField) {
        this.cavaleiroField = cavaleiroField;
    }
    public TextField getCapitaoField() {
        return capitaoField;
    }
    public void setCapitaoField(TextField capitaoField) {
        this.capitaoField = capitaoField;
    }
    public TextField getCurandeiraField() {
        return curandeiraField;
    }
    public void setCurandeiraField(TextField curandeiraField) {
        this.curandeiraField = curandeiraField;
    }
    public TextField getSolerteField() {
        return solerteField;
    }
    public void setSolerteField(TextField solerteField) {
        this.solerteField = solerteField;
    }
    
    /**
     * Volta para a tela anterior
     * @param event voltar
     */
    @FXML
    private void voltar(ActionEvent event) {
        System.out.println("Botão voltar!");
        Rpg.trocaTela("TelaInicial.fxml");
    }
    
    /**
     * Troca para a tela que exibe os exércitos criados quando a quantidade de membros no exército for igual a 100.
     * 
     * @param event 
     */
    @FXML
    private void proximo(ActionEvent event) {
        System.out.println("Botão próximo!");
        //chama o método para a obtenção da quantidade do exército
        this.getArmy(event);
        if(this.total == 100){
            Rpg.trocaTela("ExibirExercito.fxml");
        }else{
           this.mostrarTexto();
        }
    }
    
    /**
     * Exibe mensagem de erro caso a quantidade de membros no exército for diferente de 100.
     */
    public void mostrarTexto(){
        textoErro.setText("Para seguir, digite 100 personagens.");
    }
    
    /**
     * 
     * Obtém a quantidade digitada de cada personagem para o exército e chama funções da classe Exército para a criação das listas
     * @param event 
     */
    public void getArmy(ActionEvent event){
        //pega a quantidade digitada no textField de Jogar.fxml
        quantCavaleiro = Integer.parseInt(cavaleiroField.getText());
        quantCapitao = Integer.parseInt(capitaoField.getText());
        quantCurandeira = Integer.parseInt(curandeiraField.getText());
        quantSolerte = Integer.parseInt(solerteField.getText());
        
        this.total = quantCavaleiro + quantCapitao + quantCurandeira + quantSolerte;
        if(this.total == 100){
           e.criarExercito(quantCavaleiro, quantCapitao, quantCurandeira, quantSolerte);
           e2.sorteioBot();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
