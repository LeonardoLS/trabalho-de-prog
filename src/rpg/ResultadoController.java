/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 * Controller da tela que exibe dados da batalha e dos exércitos.
 * @author larac e kamillek
 */
public class ResultadoController implements Initializable {

    @FXML
    private Label dados1;
    
    @FXML
    private Label dados2;
    
    @FXML
    private Label rendidos;
    
    /**
     * Método para exibir a quantidade de personagens mortos em cada exército
     */
    public void setLabelText(){
        int cavDead = rpg.JogarController.quantCavaleiro - rpg.JogarController.e.tamCavaleiro();
        int capDead = rpg.JogarController.quantCapitao - rpg.JogarController.e.tamCapitao();
        int curDead = rpg.JogarController.quantCurandeira - rpg.JogarController.e.tamCurandeira();
        int solDead = rpg.JogarController.quantSolerte - rpg.JogarController.e.tamSolerte();
        dados1.setText("Cavaleiros mortos: " + cavDead + "\nCapitões mortos: " + capDead + "\nCurandeiras mortas: " + curDead + "\nSolertes mortos: " + solDead);
        
        int cavDead2 = rpg.Exercito.tamCav2 - rpg.JogarController.e2.tamCavaleiro();
        int capDead2 = rpg.Exercito.tamCap2 - rpg.JogarController.e2.tamCapitao();
        int curDead2 = rpg.Exercito.tamCur2 - rpg.JogarController.e2.tamCurandeira();
        int solDead2 = rpg.Exercito.tamSol2 - rpg.JogarController.e2.tamSolerte();
        dados2.setText("Cavaleiros mortos: " + cavDead2 + "\nCapitões mortos: " + capDead2 + "\nCurandeiras mortas: " + curDead2 + "\nSolertes mortos: " + solDead2);
        setRendidos();
    }
    
    /**
     * Método para exibir uma mensagem de acordo com o vencedor da batalha.
     */
    public void setRendidos(){
        int rend = rpg.Batalha.tamEx2;
        if(rpg.Batalha.venc==1){
            rendidos.setText(rend + " membros do exercito inimigo se renderam!");
        }else{
            rendidos.setText("Boa batalha, amigo!");
        }
    }
    
    /**
     * Método que encerra o RPG ao clicar no botão "Sair"
     * @param event 
     */
    @FXML
    private void sair(ActionEvent event) {
        Platform.exit();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setLabelText();
    }    
    
}
