package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * Controller da tela inicial que permite acessar o turorial, configurar ataquee defesa dos personagens e jogar
 * 
 * @author larac e kamillek
 */
public class TelaInicialController implements Initializable {
    /**
     * Troca para a tela que inicia o jogo com a entrada da quantidade de personagens do exército.
     * 
     * @param event 
     */
    @FXML
    private void jogar(ActionEvent event) {
        rpg.Personagem.configurarPersonagens();
        System.out.println("Botão jogar!");
        Rpg.trocaTela("Jogar.fxml");
    }
    
    /**
     * Troca para a tela de tutorial
     * 
     * @param event 
     */
    @FXML
    private void tutorial(ActionEvent event) {
        System.out.println("Botão tutorial!");
        Rpg.trocaTela("Tutorial.fxml");
    }
    
    /**
     * Troca para a tela de configuração de ataque e defesa dos personagens
     * 
     * @param event 
     */
    @FXML
    private void configurar(ActionEvent event){
        System.out.println("Botão configurar!");
        Rpg.trocaTela("Configurar.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
