package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * Controller da tela 2 de tutorial
 * @author larac e kamillek
 */
public class Tutorial2Controller implements Initializable {
    @FXML
    private void voltar(ActionEvent event) {
        System.out.println("Botão voltar!");
        Rpg.trocaTela("Tutorial.fxml");
    }
    
    @FXML
    private void proximo(ActionEvent event) {
        System.out.println("Botão voltar!");
        Rpg.trocaTela("Tutorial3.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }   
}
