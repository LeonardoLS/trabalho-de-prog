package rpg;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller da tela 3 de tutorial
 * @author larac e kamillek
 */
public class Tutorial4Controller {
    @FXML
    private void voltar(ActionEvent event) {
        System.out.println("Botão voltar!");
        Rpg.trocaTela("Tutorial3.fxml");
    }
    
    @FXML
    private void menu(ActionEvent event) {
        System.out.println("Botão tela inicial!");
        Rpg.trocaTela("TelaInicial.fxml");
    }
    
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
}
